## **Product Name 1:**
Predict

### **Product link**
[Product Link](https://www.clarifai.com/predict)

### **Product Short description**
Predict classifies objects, faces, people, sentiment, etc. and identifies the exact placement within a scene. We can identify leverage a suite of pre-trained models or train a custom classification model to suit our needs. We can provide some example training data and label the things or people we want to recognize in an image or video.
Categorize, sort and search documents and  use NLP to analyze text for topics, sentiment and moderate for inappropriate content. 

### **Product is combination of following features :**

Object detection
Segmentation
Face recognition
Face Detection
ID Recognition
Person Detection
Natural language processing (Text sentiment)

### **Product Company**

Clarifai

## **Product Name 2:**
IBM Video Analytics

### **Product link**
[Product Link](https://www.ibm.com/products/video-analytics?mhsrc=ibmsearch_a&mhq=Intelligent%20Video%20Analytics)

### **Product Short description**
IBM Video Analytics is an AI-based security tool that can generate a rich set of metadata describing objects, people and scenes found in video. Configurable deep learning capabilities used in Video Analytics reduce false positives. 	It can :
•	Quickly find events and objects of interest in video
•	Redact objects, faces, and entire frames in recorded video
•	Track people and objects
•	Detect changes to patterns, like abandoned objects
•	Monitor access to areas of interest
•	Send alerts when conditions detected (eg. Detecting if a person is  wearing a  mask, Detecting if social distancing is being followed, Detecting if a person is entering a no entry zone, etc)


### **Product is combination of following features :**

Object detection
Segmentation
Face recognition
Face Detection
ID Recognition
Person Detection

### **Product Company**

IBM (International Business Machines Corporation)
