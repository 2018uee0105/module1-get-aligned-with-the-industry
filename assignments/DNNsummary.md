### **DEEP LEARNING**

Deep learning is an AI function that mimics the workings of the human brain in processing data for use in detecting objects, recognizing speech, translating languages, and making decisions.

### **NEURAL NETWORKS**

##### SIMPLE NEURAL NETWORK

Neural networks are multi-layered networks of neurons, that we use to classify things, make predictions, etc. A _neuron_ has inputs and outputs, and stores data values. Neural Networks consists multiple layers :
 * Input Layer
 * Hidden Layer
 * Ouptut Layer.

 These layers are connected through _connections_ which transfer the output of a neuron to the input of another neuron. Each connection has a_weight_ that it multiplies to input and each input neuron has has its own _bias_ which is added the obtained in through the connection from being passed through the activation function.

##### DEEP NEURAL NETWORK

A DNN (Deep Neural Network) is a neural network with more than one hidden layer,which are interconnected, between the input Layer and the output layer.

![aa](images/aa.png)

### **WORKING OF DNN**

##### FORWARD PROPAGATION


* The input layer receives the training set (input signals).

* Neurons perform the following linear transformation using the weights and biases.
>x = (weight * input) + bias

* An _activation function_ is then applied on the obtained result to egt the output corresponding to that node.
>Y=Activation(Σ (weight * input) + bias)

* This process keeps repeating as the output from one hidden layer becomes the input from the consecutive hidden layer till it reaches the final output layer.
![ab](images/ab.png)

##### BACK PROPAGATION

Backpropagation is an algorithm commonly used to train neural networks  ie. it woeks to find the accurate the weights and biases for the model.

* The output obtained using forward propagation is compared to the expected output and the error is calculated.

* _Cost_ of the function is calculated using the errors from all the complete training set and averaging them.

* Finally, the weights and biases of the neurons are updated using _gradient descent_, so as to minimize the cost.

* This process is done layer by layer, back tracking, till the input layer is reached.

![ac](images/ac.png)

### **SOME IMPORTANT CONCEPTS**

#### Activation Function
It is a function of a node that defines the output of that node given an input or set of inputs.

Some common examples are:
##### Sigmoid Function

* Mathematican expression : 
> f(x) = 1/(1+e^-x)

* This function outputs the values only between the range 0 and 1.
* It was considered as a great choice for classification problems as the probability of every event always exists int the range of 0 to 1.  
![at](images/at.jpg)

##### ReLU Function

* ReLU stands for Rectified Linear Unit and is preffered over the sigmoid function.
* Mathematican expression : 
> f(x)=max(0,x)  
ie. the result is 0 (deactivated) only when the output is less than 0.
* This results in the advantage that it does not activate all the neurons at the same time.  
![ae](images/ae.png)

#### Cost Function

Cost function measures the performance of a ML model for a provided training set.
It averages the square of differences between predicted values and expected values for all the entries of the training set.  
Lesser the cost, More efficient the program.  
![af](images/af.png)

#### Gradient Descent

Gradient descent is used to minimize the cost function.  
It aims to find a local or global minima by iteratively moving in the direction of steepest descent. ie it tells the direction in which you should step to increase the function much faster.

![ag](images/ag.png)

