## **Convolutional Neural Networks**

Convolutional Neural Network is a Deep Learning algorithm which takes images as input, assigns weights and biases to various aspects/objects in the image and finally classify images according to the need. It is designed for working with two-dimensional image data, although it can be used with one-dimensional and three-dimensional data.
It is a combination of multiple building blocks, such as : 
* Convolution Layers
* Pooling Layers
* Activation Layer
* Fully Connected Layers

and is designed to automatically and adaptively learn spatial hierarchies of features through a backpropagation algorithm.  
![bb](images/bb.jpeg)

### **Convolutional Layer**

It is used to detect various edges, patterns and shapes. The main working of this layers depend upon the learnable _filters(or kernels)_.  Filter is convolved through the image with particular stride to search that particular feature. As we go deep, we encounter multiple convulation laters for for more specific features.
![bc](images/bc.png)

### **FILTERS and its features**

A filter is a matrix that moves over the input data, performs the dot product with the sub-region of input data, and gets the output as the matrix of dot products. It is used to detect patterns.

* ##### Kernel size :  
(size of the filter) is usually  an odd number so that there is a center pixel.
* ##### Padding :  
convoluted feature was observed to get reduced in dimensionality as compared to the input. This poses a big problem when a number of convolutional layers are stacked. Also, it means that the boundary  elements are not getting required importance. To solve this, padding was introduced, which adds a layer od zeros on all the edges.  
![bd](images/bd.png)
* ##### Stride :  
It defines the number of pixels that the filter slides over at each step of convolution. Default value is set to 1 but it is raised if need to reduce the input size arises.  
![be](images/be.png)

### **Pooling Layer**
To reduce the spatial size of the obtained feature map from convulational layer, _max pooling_ is done.  It reduces the parameters and model computation becomes easier. This process takes place in the pooling layer.  
In Max Pooling, the most weighted elements are considered in selected fixed smaller grids and written as one pixel values.  
![bf](images/bf.png)
### **Activation Layer**
After pooling, the output is passed through another mathematical function which is called an activation function. In this , all the negative values produced after multiplication are converted into 0's as they have no weightage in the classification. ReLU is the most common activation function.  
![bg](images/bg.png)
### **Flattening**
Flattening converts the data into a 1-dimensional feature vector for inputting it as the first layer to a DNN  (Deep Neural Network).  
![bh](images/bh.png)
### **Fully Connected Layer**
The flattened feature maps ( in the DNN) become Fully Connected Layer. Fully connected layers connect every neuron in one layer to every neuron in another layer.  
![bi](images/bi.png)
